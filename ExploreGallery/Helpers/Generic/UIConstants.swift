//
//  UIConstants.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 15/10/2020.
//

import UIKit

class UIConstants {

    // SafeArea
    
    static let topSafeAreaInset: CGFloat = {
        if let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
            return window.safeAreaInsets.top
        }
        return 0
    }()
    
    static let bottomSafeAreaInset: CGFloat = {
        if let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
            return window.safeAreaInsets.bottom
        }
        return 0
    }()
    
    static let navigationBarHeight: CGFloat = {
        if let navigationController = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController as? UINavigationController {
            return navigationController.navigationBar.frame.size.height
        }
        return 0
    }()

}
