//
//  Date+Extensions.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 18/10/2020.
//

import Foundation

extension Date {
    
    public func stringWith(fromat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = fromat
        return formatter.string(from: self)
    }
    
    public static let isoFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "it_IT")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }()
    
    /// 21/11/1988 20:04
    public func dateTimeShortString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// 21/11/1988
    public func dateShortString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// (21/11/1988, 20:04)
    public static func fromString(date: String, time: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy,HH:mm"
        return formatter.date(from: date + "," + time)
    }
    
    /// 1988/11/21
    public func dateStringAmerican() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.string(from: self)
    }
    
    /// December 31 || 31 December
    public func dateMonthString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.setLocalizedDateFormatFromTemplate("MMMMd")
        return formatter.string(from: self) // December 31 || 31 December
    }
    
    /// December 31, 2019 || 31 December 2019
    public func dateLongString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// 12:34
    public func timeShortString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// Mar
    public func month3LettersString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM"
        
        let formatted = formatter.string(from: self)
        let first = formatted.prefix(1).capitalized
        let other = formatted.suffix(formatted.count-1)
        let firstLetterUppercased = first + other
        return firstLetterUppercased
    }
    
    /// yyyy-MM-dd'T'HH:mm:ssZ
    public func isoString() -> String {
        return Date.isoFormatter.string(from: self)
    }
    
    public static func fromIso(string: String) -> Date? {
        return Date.isoFormatter.date(from: string)
    }
    
    /// 19881121_123456
    public func filenameString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMdd_HHmmss.SSSS"
        return formatter.string(from: self)
    }
    
    public func commentString() -> String {
        let now = Date()
        let components = Set<Calendar.Component>([.day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: self, to: now)
        
        let yesterday = Calendar.current.isDateInYesterday(self)
        let differenceDay = differenceOfDate.day ?? 0
        let differenceMonth = differenceOfDate.month ?? 0
        let differenceYear = differenceOfDate.year ?? 0
        
        var compareResult = ""
        
        if yesterday {
            compareResult = NSLocalizedString("date_yesterday", comment: "")
        } else if differenceMonth == 0 && differenceYear == 0 {
            switch differenceDay {
            case 0:
                compareResult = "\(NSLocalizedString("date_today_at", comment: "")) \(self.timeShortString())"
            case differenceDay where differenceDay > 1 && differenceDay < 7:
                compareResult = "\(differenceDay) \(NSLocalizedString("date_days_ago", comment: ""))"
            case differenceDay where differenceDay >= 7 && differenceDay < 14:
                compareResult = NSLocalizedString("date_one_week_ago", comment: "")
            case differenceDay where differenceDay >= 14 && differenceDay < 21:
                compareResult = NSLocalizedString("date_two_weeks_ago", comment: "")
            case differenceDay where differenceDay >= 21 && differenceDay < 30:
                compareResult = NSLocalizedString("date_three_weeks_ago", comment: "")
            default:
                break
            }
        } else if differenceYear == 0 {
            switch differenceMonth{
            case 1:
                compareResult = NSLocalizedString("date_one_month_ago", comment: "")
            case differenceMonth where differenceMonth > 1 && differenceMonth <= 12:
                compareResult = "\(differenceMonth) \(NSLocalizedString("date_months_ago", comment: ""))"
            default:
                break
            }
        } else {
            switch differenceYear {
            case 1:
                compareResult = NSLocalizedString("date_one_year_ago", comment: "")
            default:
                compareResult = "\(differenceYear) \(NSLocalizedString("date_years_ago", comment: ""))"
            }
        }
        
        return compareResult
    }
    
    public func startingInString() -> String {
        let now = Date()
        let components = Set<Calendar.Component>([.day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: now, to: self)
        
        let differenceMinutes = differenceOfDate.minute ?? 0
        let differenceHours = differenceOfDate.hour ?? 0
        let differenceDay = differenceOfDate.day ?? 0
        let differenceMonth = differenceOfDate.month ?? 0
        let differenceYear = differenceOfDate.year ?? 0
        
        var compareResult = ""
        
        // in the same week
        if differenceMonth == 0 && differenceYear == 0{
            switch differenceDay{
            case 1:
                compareResult = "1 DAY"
            case differenceDay where differenceDay > 1 && differenceDay < 7:
                compareResult = "\(differenceDay) DAYS"
            case differenceDay where differenceDay >= 7 && differenceDay < 14:
                compareResult = "1 WEEK"
            case differenceDay where differenceDay >= 14 && differenceDay < 21:
                compareResult = "2 WEEKS"
            case differenceDay where differenceDay >= 21 && differenceDay < 30:
                compareResult = "3 WEEKS"
            default:
                break
            }
        }else if differenceYear == 0{
            switch differenceMonth{
            case 1:
                compareResult = "1 MONTH"
            case differenceMonth where differenceMonth > 1 && differenceMonth <= 12:
                compareResult = "\(differenceMonth) MONTHS"
            default:
                break
            }
        }else if differenceYear > 0 {
            switch differenceYear{
            case 1:
                compareResult = "1 YEAR"
            default:
                compareResult = "\(differenceYear) YEARS"
            }
        }else if differenceHours > 0 {
            compareResult = "\(differenceHours) HOURS"
        }else {
            compareResult = "\(differenceMinutes) MINUTES"
        }
        
        return compareResult
    }
    
    /// 20/03 - 20:04
    public func lastUpdateString() -> String {
        let formatter1 = DateFormatter()
        let formatter2 = DateFormatter()
        formatter1.locale = Locale.current
        formatter1.dateFormat = "dd/MM"
        formatter2.locale = Locale.current
        formatter2.dateStyle = .none
        formatter2.timeStyle = .short // "HH:mm"
        return "\(formatter1.string(from: self)) - \(formatter2.string(from: self))"
    }
    
    /// 20 May 2019 || May 20, 2019
    public func dateMediumString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// 2019
    public func yearString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter.string(from: self)
    }
    
    public func yearInt() -> Int {
        let components = Calendar.current.dateComponents([.year], from: self)
        return components.year ?? 0
    }
    
    public func monthString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        print(formatter.string(from: self))
        return formatter.string(from: self)
    }
    
    public func monthInt() -> Int {
        let components = Calendar.current.dateComponents([.month], from: self)
        return components.month ?? 0
    }
    
    public func dayString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter.string(from: self)
    }
    
    public func dayInt() -> Int {
        let components = Calendar.current.dateComponents([.day], from: self)
        return components.day ?? 0
    }
    
    public func hourString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        return formatter.string(from: self)
    }
    
    public func hourInt() -> Int {
        let components = Calendar.current.dateComponents([.hour], from: self)
        return components.hour ?? 0
    }
    
    public func minuteString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm"
        return formatter.string(from: self)
    }
    
    public func minuteInt() -> Int {
        let components = Calendar.current.dateComponents([.minute], from: self)
        return components.minute ?? 0
    }
    
    public func smallTimeIndicator() -> String {
        let now = Date()
        let components = Set<Calendar.Component>([.day, .month, .year, .hour, .minute])
        let differenceOfDate = Calendar.current.dateComponents(components, from: self, to: now)
        
        let differenceMinutes = differenceOfDate.minute ?? 0
        let differenceHours = differenceOfDate.hour ?? 0
        let differenceDay = differenceOfDate.day ?? 0
        let differenceMonth = differenceOfDate.month ?? 0
        let differenceYear = differenceOfDate.year ?? 0
        
        var compareResult = ""
        
        // in the same week
        if differenceMonth == 0 && differenceYear == 0{
            switch differenceDay {
            case 1:
                compareResult = "1\("date_small_day".localized())"
            case differenceDay where differenceDay > 1 && differenceDay < 7:
                compareResult = "\(differenceDay)\("date_small_days".localized())"
            case differenceDay where differenceDay >= 7 && differenceDay < 14:
                compareResult = "1\("date_small_week".localized())"
            case differenceDay where differenceDay >= 14 && differenceDay < 21:
                compareResult = "2\("date_small_weeks".localized())"
            case differenceDay where differenceDay >= 21 && differenceDay < 30:
                compareResult = "3\("date_small_weeks".localized())"
            default:
                if differenceHours > 0 {
                    if differenceHours == 1 {
                        compareResult = "1\("date_small_hour".localized())"
                    } else {
                        compareResult = "\(differenceHours)\("date_small_hours".localized())"
                    }
                } else if differenceMinutes == 1 {
                    compareResult = "1\("date_small_minute".localized())"
                } else if differenceMinutes > 1 {
                    compareResult = "\(differenceMinutes)\("date_small_minutes".localized())"
                } else {
                    compareResult = "date_now".localized()
                }
            }
        } else if differenceYear > 0 {
            switch differenceYear{
            case 1:
                compareResult = "1\("date_small_year".localized())"
            default:
                compareResult = "\(differenceYear)\("date_small_years".localized())"
            }
        } else if differenceYear == 0 {
            switch differenceMonth {
            case 1:
                compareResult = "1\("date_small_month".localized())"
            case differenceMonth where differenceMonth > 1 && differenceMonth <= 12:
                compareResult = "\(differenceMonth)\("date_small_months".localized())"
            default:
                break
            }
        }
        
        return compareResult
    }
    
    /// yyyy-MM-dd
    public func dateMetadataString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: self)
    }
    
    /// yyyy-MM-dd
    public static func fromMetadataString(date: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: date)
    }
        
    public init(from date: Date, time: Date) {
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        let hour = calendar.component(.hour, from: time)
        let minutes = calendar.component(.minute, from: time)
        
        // Specify date components
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minutes
    
        self = calendar.date(from: dateComponents)!
    }
    
    /// Il 21/11/1988 alle 20:04
    public func dateTimeDescriptionString() -> String {
        let formatter1 = DateFormatter()
        let formatter2 = DateFormatter()
        formatter1.locale = Locale.current
        formatter1.dateStyle = .short
        formatter1.timeStyle = .none
        formatter2.locale = Locale.current
        formatter2.dateStyle = .none
        formatter2.timeStyle = .short // "HH:mm"
        return "date_on".localized() + " \(formatter1.string(from: self)) " + "date_at".localized().lowercased() + " \(formatter2.string(from: self))"
        
    }
}

extension TimeInterval {
    public func hoursMinString() -> String {
        let interval = Int(self)
        let hours = interval / 3600
        let minutes = (interval % 3600) / 60
        return String(format: "%02d:%02d", hours, minutes)
    }
}
