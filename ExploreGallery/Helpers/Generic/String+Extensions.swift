//
//  String+Extensions.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit

extension String {
    static func localized(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    public func uppercaseFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    public mutating func uppercaseFirstLetter() {
        self = self.uppercaseFirstLetter()
    }
    
    func stringByReplacingFirstOccurrenceOfString(target: String, withString replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
}
