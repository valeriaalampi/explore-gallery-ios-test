//
//  UIAlertController+EG.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Foundation
import UIKit

extension UIAlertController {
    
    //Set title font and title color
    func customizeTitle(font: UIFont?, color: UIColor?) {
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont],
                                          range: NSMakeRange(0, title.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],
                                          range: NSMakeRange(0, title.count))
        }
        
        self.setValue(attributeString, forKey: "attributedTitle")
    }
    
    //Set message font and message color
    func customizeBody(font: UIFont?, color: UIColor?) {
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font: messageFont],
                                          range: NSMakeRange(0, message.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor: messageColorColor],
                                          range: NSMakeRange(0, message.count))
        }
        
        self.setValue(attributeString, forKey: "attributedMessage")
    }
    
    func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = .gray
            let subviews = contentView.subviews
            if !subviews.isEmpty {
                for subview in subviews {
                    subview.backgroundColor = .lightGray
                }
            }
        }
    }
    
    func setTint(color: UIColor) {
        self.view.tintColor = color
    }
    
}
