//
//  UIColor+EG.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit

// Palette
extension UIColor {
    
    @nonobjc public class var primary: UIColor {
        return .lightishBlue
    }
    
    @nonobjc public class var lightishBlue: UIColor {
        return UIColor(named: "lightishBlue")!
    }
}
