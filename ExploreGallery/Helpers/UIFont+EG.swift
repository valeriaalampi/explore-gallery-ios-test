//
//  UIFont+EG.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Foundation
import UIKit

extension UIFont {
    
    // MARK: - Styles
    
    // Regular
    class var regular10: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 10.0)!
    }
    
    class var regular11: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 11.0)!
    }
    
    class var regular12: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 12.0)!
    }
    
    class var regular13: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 13.0)!
    }
    
    class var regular14: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 14.0)!
    }
    
    class var regular15: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 15.0)!
    }
    
    class var regular16: UIFont {
        return UIFont(name: "AvenirNext-Regular", size: 16.0)!
    }

    // Medium
    class var medium10: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 10.0)!
    }
    
    class var medium13: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 13.0)!
    }
    
    class var medium14: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 14.0)!
    }
    
    class var medium15: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 15.0)!
    }
    
    class var medium16: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 16.0)!
    }
    
    class var medium17: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 17.0)!
    }
    
    class var medium20: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 20.0)!
    }
    
    class var medium25: UIFont {
        return UIFont(name: "AvenirNext-Medium", size: 25.0)!
    }
    
    // Bold
    class var bold13: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 13.0)!
    }
    
    class var bold14: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 14.0)!
    }
    
    class var bold15: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 15.0)!
    }
    
    class var bold17: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 17.0)!
    }
    
    class var bold20: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 20.0)!
    }
    
    class var bold25: UIFont {
        return UIFont(name: "AvenirNext-Bold", size: 25.0)!
    }
    
    // Black
    class var black13: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 13.0)!
    }
    
    class var black14: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 14.0)!
    }
    
    class var black15: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 15.0)!
    }
    
    class var black17: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 17.0)!
    }
    
    class var black20: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 20.0)!
    }
    
    class var black25: UIFont {
        return UIFont(name: "AvenirNext-Heavy", size: 25.0)!
    }

}
