//
//  WebAPIError.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 15/10/2020.
//

import Foundation

enum WebAPIError: Error {
    case resultMissing
    case parameterMissing
}

extension WebAPIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .resultMissing:
            return "Result missing in server response"
        case .parameterMissing:
            return "Parameter missing in request"
        }
    }
}
