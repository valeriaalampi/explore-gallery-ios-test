//
//  WebAPIService.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 15/10/2020.
//


import Foundation
import RxSwift
import RxAlamofire

protocol WebAPIServiceProtocol {
    
    // MARK:- Reddit Photos
    
    func getItems(for keyword: String) -> Observable<[RedditItem]>
    
}

class WebAPIService: WebAPIServiceProtocol {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    
    // MARK: Private
//    private var cacheService: CacheServiceProtocol
    private let webConcurrentScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
//    init(cacheService: CacheServiceProtocol) {
//        self.cacheService = cacheService
//    }
    init() {
    }
    
    // MARK: Public
    func getItems(for keyword: String) -> Observable<[RedditItem]> {
        let stringUrl = "https://www.reddit.com/r/\(keyword)/top.json"
        log("WebTask: GET \(stringUrl)")
        
        return RxAlamofire.request(.get, stringUrl, parameters: nil, headers: nil)
            .subscribeOn(webConcurrentScheduler)
            .responseJSON()
            .flatMap { (response) -> Observable<[RedditItem]> in
                guard let responseData = response.data else {
                    log("WebTask: GET \(stringUrl) - ERROR")
                    return Observable.error(response.error ?? WebAPIError.resultMissing)
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .millisecondsSince1970 // it doesn't work (?)
                    let photosResponse = try decoder.decode(RedditResponse.self, from: responseData)
                    
                    log("WebTask: GET \(stringUrl) - SUCCESS")
                    return Observable.just(photosResponse.data?.children ?? [])
                } catch let error {
                    log("WebTask: GET \(stringUrl) - ERROR")
                    print(error.localizedDescription)
                    return Observable.just([])
                }
            }
    }
    
    
    // MARK: Private
    
}
