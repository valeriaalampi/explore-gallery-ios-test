//
//  AppCoordinator.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import RxSwift

class AppCoordinator: Coordinator {
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    var children: [Coordinator] = []
    // MARK: Private
    private let disposeBag = DisposeBag()
    private let navigationController: EGNavigationController
    private let exploreCoordinator: ExploreCoordinator

    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: EGNavigationController) {
        self.navigationController = navigationController
        
        exploreCoordinator = ExploreCoordinator(navigationController: navigationController)
        
        setupBindings()
    }

    // MARK: Public
    
    func start() {
        // Home
        add(child: exploreCoordinator)
        exploreCoordinator.start()
    }

    func showAlert(_ alert: UIAlertController) {
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Private
    private func setupBindings() {
        //
        
    }

}
