//
//  Coordinator.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Foundation

/**
 Object that manages navigation
*/
protocol Coordinator: AnyObject {
    // List of childern coordinators of this coordinator
    // This reference prevent them from being deallocated by Garbage Collector
    var children: [Coordinator] { get set }
    
    // Coordinator start function
    func start()
}

/**
 Utilities
 */
extension Coordinator {
    func add(child: Coordinator) {
        children.append(child)
    }
    
    func add(children: [Coordinator]) {
        for child in children {
            self.children.append(child)
        }
    }
    
    func remove(child: Coordinator) {
        children = children.filter { $0 !== child }
    }
    
    func remove(children: [Coordinator]) {
        for child in children {
            self.children = children.filter { $0 !== child }
        }
    }
    
    func addAndStart(child: Coordinator) {
        children.append(child)
        child.start()
    }
    
}
