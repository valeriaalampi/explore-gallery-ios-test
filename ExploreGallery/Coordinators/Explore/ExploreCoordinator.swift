//
//  ExploreCoordinator.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit

protocol ExploreCoordinatorDelegate: class {
    func exploreCoordinatorDelegateOnDismissed(_ documentsCoordinator: ExploreCoordinator)
}

class ExploreCoordinator: Coordinator {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    weak var delegate: ExploreCoordinatorDelegate?
    var children: [Coordinator] = []
    
    var innerStack: [UIViewController] = []
    
    // MARK: Private
    private let parentNavigationController: EGNavigationController
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: EGNavigationController) {
        self.parentNavigationController = navigationController
    }
    
    // MARK: Public
    func start() {
        // If it's the first viewController in navigation stack
        if innerStack.isEmpty {
            innerStack = [exploreFactory()]
        }
        parentNavigationController.pushViewController(innerStack[0], animated: true)
        
    }
    
    // MARK: Private
    private func exploreFactory() -> UIViewController {
        var exploreVm = AppContainer.shared.resolve(ExploreViewModelProtocol.self)!
        exploreVm.flowDelegate = self
        let exploreVc = ExploreViewController(viewModel: exploreVm)
        return exploreVc
    }

    private func itemDetailFactory(item: RedditItem) -> UIViewController {
        var itemDetailVm = AppContainer.shared.resolve(ItemDetailViewModelProtocol.self, argument: item)!
        itemDetailVm.flowDelegate = self
        let itemDetailVc = ItemDetailViewController(viewModel: itemDetailVm)
        return itemDetailVc
    }
}


// MARK: - Flow Delegates
extension ExploreCoordinator: ExploreFlowDelegate, ItemDetailFlowDelegate {
    
    // Commons
    func showAlert(_ alert: UIAlertController) {
        parentNavigationController.present(alert, animated: true)
    }
    
    // Item detail
    func showItemDetail(_ item: RedditItem) {
        debugPrint("[PRESENTING ITEM DETAIL: \(item)]")
        
        let itemDetailVc = itemDetailFactory(item: item)
        innerStack.append(itemDetailVc)
        parentNavigationController.pushViewController(itemDetailVc, animated: true)
    }
    
    func popItemDetail() {
        delegate?.exploreCoordinatorDelegateOnDismissed(self)
        
        guard innerStack.count > 1 else { return }
        _ = innerStack.popLast()
        parentNavigationController.popViewController(animated: true)
    }
    
}
