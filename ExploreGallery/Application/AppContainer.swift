//
//  AppContainer.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Swinject

class AppContainer {
    
    static let shared: Container = {
        
        let container = Container()
        
        //************* HELPERS **************//
        
        //************* SERVICES *************//
        
        // WebAPIService
        container.register(WebAPIServiceProtocol.self) { (r: Resolver) in
//            let cacheService = r.resolve(CacheServiceProtocol.self)!
//            return WebAPIService(cacheService: cacheService)
            return WebAPIService()
        }
        
        //************* VIEWMODELS *************//
        
        // Explore
        container.register(ExploreViewModelProtocol.self) { _ in
            ExploreViewModel(container: container)
        }
        
        // Item Detail
        container.register(ItemDetailViewModelProtocol.self) { (r: Resolver, item: RedditItem) in
            ItemDetailViewModel(container: container, item: item)
        }
        
        return container
    }()
}
