//
//  RedditItem.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Foundation
import RxDataSources

public struct RedditItem: Codable {
    
    public var kind: String
    public var data: SingleItemData
    
    public struct SingleItemData: Codable {
        var id: String
        var subreddit: String
        var author: String
        var title: String
        var created: Double
        var created_utc: Double
        
        var thumbnail: String?
        var url: String?

        var score: Int?
        var likes: Int?
        var num_comments: Int?
        var is_video: Bool?
    }
    
    public func hasImage() -> Bool {
        if let imageUrl = URL(string: self.data.url ?? ""), imageUrl.isImage {
            return true
        } else if let imageUrl = URL(string: self.data.thumbnail ?? ""), imageUrl.isImage {
            return true
        }
        return false
    }
}


extension RedditItem: IdentifiableType, Equatable {
    
    public typealias Identity = String
    
    public var identity: String {
        get {
            self.data.id
        }
    }
    
    public static func == (lhs: RedditItem, rhs: RedditItem) -> Bool {
        return lhs.data.id == rhs.data.id
    }
}

extension RedditItem: Hashable {
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(data.id)
    }
    
}

public struct RedditResponse: Codable {
    let kind: String
    let data: RedditData?
}

public struct RedditData: Codable {
    let dist: Int
    let children: [RedditItem]?
}

// Example json
/*
 {
   "kind": "Listing",
   "data": {
     "modhash": "",
     "dist": 1,
     "children": [
       {
         "kind": "t3",
         "data": {
           "approved_at_utc": null,
           "subreddit": "Sky",
           "selftext": "",
           "author_fullname": "t2_5zdml7qh",
           "saved": false,
           "mod_reason_title": null,
           "gilded": 0,
           "clicked": false,
           "title": "hyderabad this evening",
           "link_flair_richtext": [],
           "subreddit_name_prefixed": "r/Sky",
           "hidden": false,
           "pwls": null,
           "link_flair_css_class": null,
           "downs": 0,
           "top_awarded_type": null,
           "hide_score": false,
           "name": "t3_g3xcrt",
           "quarantine": false,
           "link_flair_text_color": "dark",
           "upvote_ratio": 0.95,
           "author_flair_background_color": null,
           "subreddit_type": "restricted",
           "ups": 28,
           "total_awards_received": 0,
           "media_embed": {},
           "author_flair_template_id": null,
           "is_original_content": false,
           "user_reports": [],
           "secure_media": null,
           "is_reddit_media_domain": true,
           "is_meta": false,
           "category": null,
           "secure_media_embed": {},
           "link_flair_text": null,
           "can_mod_post": false,
           "score": 28,
           "approved_by": null,
           "author_premium": false,
           "thumbnail": "",
           "edited": false,
           "author_flair_css_class": null,
           "author_flair_richtext": [],
           "gildings": {},
           "content_categories": null,
           "is_self": false,
           "mod_note": null,
           "created": 1587281302.0,
           "link_flair_type": "text",
           "wls": null,
           "removed_by_category": null,
           "banned_by": null,
           "author_flair_type": "text",
           "domain": "i.redd.it",
           "allow_live_comments": false,
           "selftext_html": null,
           "likes": null,
           "suggested_sort": null,
           "banned_at_utc": null,
           "url_overridden_by_dest": "https://i.redd.it/u9978fz2vnt41.jpg",
           "view_count": null,
           "archived": false,
           "no_follow": false,
           "is_crosspostable": false,
           "pinned": false,
           "over_18": false,
           "all_awardings": [],
           "awarders": [],
           "media_only": false,
           "can_gild": false,
           "spoiler": false,
           "locked": false,
           "author_flair_text": null,
           "treatment_tags": [],
           "visited": false,
           "removed_by": null,
           "num_reports": null,
           "distinguished": null,
           "subreddit_id": "t5_2qlyj",
           "mod_reason_by": null,
           "removal_reason": null,
           "link_flair_background_color": "",
           "id": "g3xcrt",
           "is_robot_indexable": true,
           "report_reasons": null,
           "author": "organizedchaos8",
           "discussion_type": null,
           "num_comments": 1,
           "send_replies": true,
           "whitelist_status": null,
           "contest_mode": false,
           "mod_reports": [],
           "author_patreon_flair": false,
           "author_flair_text_color": null,
           "permalink": "/r/Sky/comments/g3xcrt/hyderabad_this_evening/",
           "parent_whitelist_status": null,
           "stickied": false,
           "url": "https://i.redd.it/u9978fz2vnt41.jpg",
           "subreddit_subscribers": 959,
           "created_utc": 1587252502.0,
           "num_crossposts": 0,
           "media": null,
           "is_video": false
         }
       }
     ],
     "after": null,
     "before": null
   }
 }
 */
