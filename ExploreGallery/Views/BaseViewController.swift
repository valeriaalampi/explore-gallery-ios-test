//
//  BaseViewController.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewController: UIViewController {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    let hudIsVisible = BehaviorRelay<Bool>(value: false)
    
    var nav: EGNavigationController? {
        return navigationController as? EGNavigationController
    }
    lazy var backBtn = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .plain, target: self, action: nil)
    var navBarIsVisible: Bool = false
    var navBarIsTransparent: Bool = false
    var navBarIsTranslucent: Bool = false
    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            nav?.statusBarStyle = statusBarStyle
        }
    }
    let disposeBag = DisposeBag()
    
    // MARK: Private
    private let hud = SpinnerViewController()
    
    // MARK: - Methods
    // MARK: Class
    
    
    
    // MARK: Lifecycle
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status bar settings
        statusBarStyle = .default
        
        navigationItem.hidesBackButton = true
        if navBarIsTransparent {
            navBarIsTranslucent = true
        }
        
        setupUi()
        setupConstraints()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // NAVIGATION BAR
        let barButtonItemAttributes = [
            NSAttributedString.Key.font: UIFont.medium14,
            NSAttributedString.Key.foregroundColor: UIColor.primary,
        ]
        let barButtonItemDisabledAttributes = [
            NSAttributedString.Key.font: UIFont.medium14,
            NSAttributedString.Key.foregroundColor: UIColor.gray,
        ]
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .normal)
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .highlighted)
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonItemDisabledAttributes, for: .disabled)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .highlighted)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonItemDisabledAttributes, for: .disabled)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .normal)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonItemAttributes, for: .highlighted)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonItemDisabledAttributes, for: .disabled)
        
        nav?.setNavigationBarHidden(!navBarIsVisible, animated: true)
        nav?.navigationBar.isTranslucent = navBarIsTranslucent ? true : false
        nav?.navigationBar.barTintColor = navBarIsTransparent ? .clear : .white
        nav?.navigationBar.setBackgroundImage(navBarIsTransparent ? UIImage() : nil, for: .default)

        
    }
    
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    // Override this method to initialize the UI
    func setupUi() {
        
    }
    
    // Override this method to setup the layout constraints
    func setupConstraints() {
        
    }
    
    // Override this method to setup the bindings
    func setupBindings() {
        
        hudIsVisible.asDriver()
            .skip(1)
            .drive(onNext: { [weak self] hudIsVisible in
                if hudIsVisible {
                    self?.showHud()
                } else {
                    self?.hideHud()
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    // MARK: Private
    
    private func showHud() {
        DispatchQueue.main.async {
            self.addChild(self.hud)
            self.hud.view.frame = self.view.frame
            self.view.addSubview(self.hud.view)
            self.hud.didMove(toParent: self)
        }
        
    }
    
    private func hideHud() {
        DispatchQueue.main.async {
            self.hud.willMove(toParent: nil)
            self.hud.view.removeFromSuperview()
            self.hud.removeFromParent()
        }
        
    }

    // MARK: Protocol conformance

}
