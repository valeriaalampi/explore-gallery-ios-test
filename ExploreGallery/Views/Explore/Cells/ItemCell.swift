//
//  ItemCell.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 15/10/2020.
//

import UIKit
import RxSwift
import Kingfisher

class ItemCell: UICollectionViewCell {
    // MARK: Public
    
    // MARK: - Properties
    // MARK: Class
    static let reuseId = "ItemCell"
    
    // MARK: Public
    var disposeBag = DisposeBag()
    var item: RedditItem?
    
    // MARK: Private
    private let imageView = UIImageView()
    
    
    // MARK: - Methods
    // MARK: Class
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        item = nil
        
        super.init(frame: .zero)
        
        // Thumbnail image
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        // Add subviews
        addSubview(imageView)
        
        // Constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.leftAnchor.constraint(equalTo: self.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: self.rightAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
        item = nil
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    public func configure(_ item: RedditItem) {
        self.item = item
        
        // Loading image
        var urlImage = URL(fileURLWithPath: "")
        if var imageUrl = URL(string: item.data.thumbnail ?? ""), imageUrl.isImage {
            if imageUrl.absoluteString.starts(with: "//") {
                imageUrl = URL(string: "https:\(imageUrl.absoluteString)") ?? URL(fileURLWithPath: "")
            }
            urlImage = imageUrl
        } else if var imageUrl = URL(string: item.data.url ?? ""), imageUrl.isImage {
            if imageUrl.absoluteString.starts(with: "//") {
                imageUrl = URL(string: "https:\(imageUrl.absoluteString)") ?? URL(fileURLWithPath: "")
            }
            urlImage = imageUrl
        }
        if urlImage != URL(fileURLWithPath: "") {
            self.loadImage(urlImage, to: self.imageView)
        } else {
            // Shoudn't be here since I've filtered only items containing an image
        }
        
    }
    
    // MARK: Private
    func loadImage(_ urlImage: URL, to imageView: UIImageView) {
        
        log("DownloadTask: IMAGE \(urlImage)")
        imageView.kf.setImage(
            with: urlImage,
            placeholder: UIImage(named: "placeholderImage"), // placeholder here
            options: [
                .scaleFactor(UIScreen.main.scale),
                .cacheOriginalImage
            ], completionHandler:  { result in
                switch result {
                    case .success(let value):
                        log("DownloadTask: IMAGE \(urlImage) - SUCCESS - cache: \(value.cacheType)")
                    case .failure(let error):
                        log("DownloadTask: IMAGE \(urlImage) - ERROR - \(error.localizedDescription)")
                        imageView.image = UIImage(named: "placeholderImage") // placeholder here
                }
            })
    }
    
    // MARK: Protocol conformance
    
    
}
