//
//  ItemDetailViewController.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 17/10/2020.
//

import UIKit
import RxSwift
import RxCocoa

final class ItemDetailViewController: BaseViewController {
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    let viewModel: ItemDetailViewModelProtocol
    
    // MARK: Private
    let imageView = UIImageView()
    let titleLabel = UILabel()
    let authorLabel = UILabel()
    let dateLabel = UILabel()
    let scoreLabel = UILabel()
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("ItemDetailViewController must be initialized from init(viewModel:)")
    }
    
    init(viewModel: ItemDetailViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init()
    }
    
    // MARK: Public
    override func setupUi() {
        super.setupUi()
        
        view.backgroundColor = .systemGray6
        
        // Navigation bar
        navBarIsVisible = true
        navigationItem.hidesBackButton = false
        
        // Image view
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        // Title label
        titleLabel.font = .regular12
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 2
        titleLabel.lineBreakMode = .byWordWrapping
        
        // Author label
        authorLabel.font = .bold13
        authorLabel.textColor = .black
        
        // Date label
        dateLabel.font = .regular12
        dateLabel.textColor = .darkGray
        
        // Date label
        scoreLabel.font = .regular12
        scoreLabel.textColor = .darkGray
        scoreLabel.textAlignment = .right
        
        // Add subviews
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(authorLabel)
        view.addSubview(dateLabel)
        view.addSubview(scoreLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        // Constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let imageViewConstraints = [
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ]
        NSLayoutConstraint.activate(imageViewConstraints)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let titleLabelConstraints = [
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]
        NSLayoutConstraint.activate(titleLabelConstraints)
        
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        let authorLabelConstraints = [
            authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            authorLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            authorLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]
        NSLayoutConstraint.activate(authorLabelConstraints)
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        let dateLabelConstraints = [
            dateLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 4),
            dateLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            dateLabel.rightAnchor.constraint(equalTo: scoreLabel.leftAnchor, constant: -16),
            dateLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(UIConstants.bottomSafeAreaInset + 16))
        ]
        NSLayoutConstraint.activate(dateLabelConstraints)
        
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        let scoreLabelConstraints = [
            scoreLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 4),
            scoreLabel.leftAnchor.constraint(equalTo: dateLabel.rightAnchor, constant: 16),
            scoreLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            scoreLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(UIConstants.bottomSafeAreaInset + 16))
        ]
        NSLayoutConstraint.activate(scoreLabelConstraints)
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        // Hud
        viewModel.isLoading
            .asDriver()
            .drive(hudIsVisible)
            .disposed(by: disposeBag)
        
        // Retrieve image
        viewModel.imageURL.asObservable()
            .compactMap { $0 }
            .subscribe(onNext: { self.loadImage($0, to: self.imageView) })
            .disposed(by: disposeBag)
        
        // Title
        viewModel.item.asObservable()
            .map { $0.data.title }
            .bind(to: self.navigationItem.rx.title)
            .disposed(by: disposeBag)
        
        viewModel.item.asObservable()
            .map { "\"\($0.data.title)\"" }
            .bind(to: self.titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        // Author
        viewModel.item.asObservable()
            .map { $0.data.author }
            .bind(to: self.authorLabel.rx.text)
            .disposed(by: disposeBag)
        
        // Date
        viewModel.item.asObservable()
            .map { Date(timeIntervalSince1970: $0.data.created_utc).dateTimeDescriptionString() }
            .bind(to: self.dateLabel.rx.text)
            .disposed(by: disposeBag)
        
        // Score
        viewModel.item.asObservable()
            .map { "🔝 \($0.data.score ?? 0)" } // ⭐️
            .bind(to: self.scoreLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
 
    // MARK: Private
    private func loadImage(_ urlImage: URL, to imageView: UIImageView) {
        log("DownloadTask: IMAGE \(urlImage)")
        
        self.viewModel.isLoading.accept(true)
        imageView.kf.setImage(
            with: urlImage,
            placeholder: UIImage(named: "placeholderImage"), // placeholder here
            options: [
                .scaleFactor(UIScreen.main.scale),
                .cacheOriginalImage
            ], completionHandler:  { result in
                switch result {
                    case .success(let value):
                        log("DownloadTask: IMAGE \(urlImage) - SUCCESS - cache: \(value.cacheType)")
                    case .failure(let error):
                        log("DownloadTask: IMAGE \(urlImage) - ERROR - \(error.localizedDescription)")
                        imageView.image = UIImage(named: "placeholderImage") // placeholder here
                }
                
                self.viewModel.isLoading.accept(false)
            })
    }
}
