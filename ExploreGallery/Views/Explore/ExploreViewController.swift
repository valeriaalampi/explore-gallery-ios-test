//
//  ExploreViewController.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit
import RxSwift
import RxDataSources

class ExploreViewController: BaseViewController {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    let viewModel: ExploreViewModelProtocol
    
    // MARK: Private
    private var tap: UITapGestureRecognizer?
    private var press: UILongPressGestureRecognizer?
    private var swipe: UISwipeGestureRecognizer?
    private var pan: UIPanGestureRecognizer?
    
    // UIFields
    private let searchBar = UISearchBar()
    private let overlayView = UIView()
    private let emptyLabel = UILabel()
    private var itemCollectionView: UICollectionView!
    
    // Datasources
    private var dataSource: RxCollectionViewSectionedAnimatedDataSource<AnimatableSectionModel<String, RedditItem>>!
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("ExploreViewController must be initialized from init(viewModel:)")
    }
    
    init(viewModel: ExploreViewModelProtocol) {
        self.viewModel = viewModel
        super.init()
        
        navBarIsVisible = true
        navBarIsTranslucent = true
        
        statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
        tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        press = UILongPressGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        swipe = UISwipeGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        pan = UIPanGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.nav?.setNavigationBarTitle(title: "explore_screen_title".localized().uppercased())
        navigationItem.backButtonTitle = ""
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.setupConstraints()
    }
    
    
    // MARK: Public
    override func setupUi() {
        super.setupUi()
        
        // Search bar
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "search_placeholder".localized()
        searchBar.backgroundColor = .white
        
        /// TextField Color Customization
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .black
        textFieldInsideSearchBar?.font = UIFont.regular13
        
        /// Placeholder Customization
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.font = UIFont.regular13
        
        // Overlay that appears when keyboard is open
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        overlayView.isHidden = true
        overlayView.isUserInteractionEnabled = false
        
        // Label for empty view
        emptyLabel.font = .regular12
        emptyLabel.textColor = .darkGray
        emptyLabel.numberOfLines = 2
        emptyLabel.lineBreakMode = .byWordWrapping
        emptyLabel.textAlignment = .center
        
        // Collection View
        itemCollectionView = UICollectionView(frame: .zero, collectionViewLayout: ColumnFlowLayout())
        itemCollectionView.backgroundColor = .clear
        itemCollectionView.clipsToBounds = false
        itemCollectionView.showsVerticalScrollIndicator = false
        itemCollectionView.showsHorizontalScrollIndicator = false
        itemCollectionView.register(ItemCell.self, forCellWithReuseIdentifier: ItemCell.reuseId)
        
        // Datasource styling
        dataSource = RxCollectionViewSectionedAnimatedDataSource<AnimatableSectionModel<String, RedditItem>>(configureCell: { (dataSource, collectionView, indexPath, item) in
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCell.reuseId, for: indexPath) as? ItemCell else {
                return UICollectionViewCell(frame: .zero)
            }
            cell.configure(item)
            return cell
        })
        
        // Adding views
        view.addSubview(emptyLabel)
        view.addSubview(itemCollectionView)
        view.addSubview(searchBar)
        view.addSubview(overlayView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()

        // Constraints
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            searchBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            searchBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
        ])
        
        itemCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemCollectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            itemCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            itemCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            itemCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -UIConstants.bottomSafeAreaInset)
        ])
        
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            emptyLabel.centerXAnchor.constraint(equalTo: itemCollectionView.centerXAnchor),
            emptyLabel.centerYAnchor.constraint(equalTo: itemCollectionView.centerYAnchor)
        ])
        
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            overlayView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            overlayView.leftAnchor.constraint(equalTo: view.leftAnchor),
            overlayView.rightAnchor.constraint(equalTo: view.rightAnchor),
            overlayView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -UIConstants.bottomSafeAreaInset)
        ])
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        // Hud
        viewModel.isLoading
            .asDriver()
            .drive(hudIsVisible)
            .disposed(by: disposeBag)
        
        // Search text
        searchBar.rx.text
            .bind(to: viewModel.searchText)
            .disposed(by: disposeBag)
        
        // No results message
        viewModel.itemsDownloaded.asObservable()
            .filter { $0.count == 0 }
            .skip(1)
            .map { _ in "explore_no_results".localized() }
            .bind(to: self.emptyLabel.rx.text)
            .disposed(by: disposeBag)
        /// Hide empty message if there are results
        viewModel.itemsDownloaded.asObservable()
            .map { $0.count != 0 }
            .bind(to: self.emptyLabel.rx.isHidden)
            .disposed(by: disposeBag)
        /// Hide empty message if results are loading
        viewModel.isLoading
            .bind(to: self.emptyLabel.rx.isHidden)
            .disposed(by: disposeBag)
    
        // Datasource
        viewModel.sections
            .drive(itemCollectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        // Tap on collection items
        itemCollectionView.rx.modelSelected(RedditItem.self)
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.itemDetailTap)
            .disposed(by: disposeBag)
        
    }
    
    public func isOverlayHidden() -> Bool {
        return overlayView.isHidden
    }
    
    // MARK: Private
    @objc private func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc private func keyboardWillAppear() {
        
        if let tap = tap, let press = press, let swipe = swipe, let pan = pan {
            view.addGestureRecognizer(tap)
            view.addGestureRecognizer(press)
            view.addGestureRecognizer(swipe)
            view.addGestureRecognizer(pan)
        }
        
        guard overlayView.isHidden else { return }
        overlayView.alpha = 0
        overlayView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.overlayView.alpha = 1
        })
    }
    
    @objc private func keyboardWillDisappear() {
        
        if let tap = tap, let press = press, let swipe = swipe, let pan = pan {
            view.removeGestureRecognizer(tap)
            view.removeGestureRecognizer(press)
            view.removeGestureRecognizer(swipe)
            view.removeGestureRecognizer(pan)
        }
        
        guard !overlayView.isHidden else { return }
        overlayView.alpha = 1
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.overlayView.alpha = 0
        }) { [weak self] _ in
            self?.overlayView.isHidden = true
        }
    }
}

extension ExploreViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchText.onNext(searchText)
    }
}
