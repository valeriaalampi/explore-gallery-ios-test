//
//  EGNavigationController.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit


class EGNavigationController: UINavigationController {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    // MARK: Private
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup view
        view.backgroundColor = .white
        view.tintColor = .primary
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    func setNavigationBarTitle(title: String, hasClearColor: Bool = false) {
        
        navigationBar.topItem?.title = title
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.medium13,
            NSAttributedString.Key.foregroundColor: hasClearColor ? UIColor.clear : UIColor.black,
        ]
        
    }
    
    func setNavigationBarTitle(title: String, color: UIColor) {
        
        navigationBar.topItem?.title = title
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.medium13,
            NSAttributedString.Key.foregroundColor: color,
        ]
        
    }
    
    func setNavigationBarTitleAndSubtitle(title: String, subtitle: String) {
        
        let titleLabel = UILabel()
        titleLabel.text = title.uppercased()
        titleLabel.font = UIFont.medium13
        titleLabel.textColor = UIColor.black

        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle.uppercaseFirstLetter()
        subtitleLabel.font = UIFont.regular13
        subtitleLabel.textColor = UIColor.systemGray

        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.distribution = .equalCentering
        stackView.alignment = .center
        stackView.axis = .vertical
        
        navigationBar.topItem?.titleView = stackView
    }

    
    // MARK: Private
    
    // MARK: Protocol conformance
    
}
