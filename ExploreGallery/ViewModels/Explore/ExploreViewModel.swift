//
//  ExploreViewModel.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import Swinject
import RxSwift
import RxCocoa
import RxDataSources

protocol ExploreFlowDelegate: class {
    func showItemDetail(_ item: RedditItem)
    func showAlert(_ alert: UIAlertController)
}

protocol ExploreViewModelProtocol: BaseViewModelProtocol {

    // Flow
    var flowDelegate: ExploreFlowDelegate? { get set }
    
    // Outputs
    var itemsDownloaded: Driver<[RedditItem]> { get }
    var sections: Driver<[AnimatableSectionModel<String, RedditItem>]> { get }
    
    // Inputs
    var searchText: AnyObserver<String?> { get }
    var itemDetailTap: AnyObserver<RedditItem?> { get }

    // Bidirectionals
    
}


class ExploreViewModel: BaseViewModel, ExploreViewModelProtocol {

    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    //Flow
    weak var flowDelegate: ExploreFlowDelegate?

    // Outputs
    let itemsDownloaded: Driver<[RedditItem]>
    let sections: Driver<[AnimatableSectionModel<String, RedditItem>]>
    
    // Inputs
    let searchText: AnyObserver<String?>
    let itemDetailTap: AnyObserver<RedditItem?>

    // Bidirectionals
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    private let webConcurrentScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    override init(container: Container) {
        
        //Outputs
        let itemsDownloadedBr = BehaviorRelay<[RedditItem]>(value: [])
        itemsDownloaded = itemsDownloadedBr.asDriver()
        
        let sectionsBr = BehaviorRelay<[AnimatableSectionModel<String, RedditItem>]>(value: [])
        sections = sectionsBr.asDriver()
        
        //Inputs
        let searchTextPs = PublishSubject<String?>()
        searchText = searchTextPs.asObserver()
        
        let itemDetailTapPs = PublishSubject<RedditItem?>()
        itemDetailTap = itemDetailTapPs.asObserver()
        
        super.init(container: container)
        
        // Bindings & Behavior
        
        // Retrieve items from search text
        searchTextPs.asObservable()
            .map { $0?.removingWhitespaces().lowercased() ?? "" }
            .filter { !$0.isEmpty }
            .distinctUntilChanged()
            .debug("search text")
            .do(onNext: { _ in self.isLoading.accept(true) })
            .flatMap { self.webAPIService.getItems(for: $0) }
            .observeOn(webConcurrentScheduler)
            .do(onNext: { _ in self.isLoading.accept(false) })
            .catchErrorJustReturn([])
            .bind(to: itemsDownloadedBr)
            .disposed(by: disposeBag)
        /// Clean results
        searchTextPs.asObservable()
            .map { $0?.removingWhitespaces().lowercased() ?? "" }
            .filter { $0.isEmpty }
            .map { _ in [] }
            .bind(to: itemsDownloadedBr)
            .disposed(by: disposeBag)
        
        // Section creation
        itemsDownloaded.asObservable()
            .map { $0.filter { $0.hasImage() } }
            .map { [AnimatableSectionModel(model: "items", items: $0)] }
            .bind(to: sectionsBr)
            .disposed(by: disposeBag)
        
        // Item detail
        itemDetailTapPs
            .asObservable()
            .compactMap { $0 }
            .bind { self.flowDelegate?.showItemDetail($0) }
            .disposed(by: disposeBag)
    }
    
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    
    // MARK: Private
    
    
    // MARK: Protocol conformance
    
}
