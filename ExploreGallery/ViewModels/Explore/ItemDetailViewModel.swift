//
//  ItemDetailViewModel.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 17/10/2020.
//

import Swinject
import RxSwift
import RxCocoa

protocol ItemDetailFlowDelegate: class {
    func popItemDetail()
    func showAlert(_ alert: UIAlertController)
}

protocol ItemDetailViewModelProtocol: BaseViewModelProtocol {
    // Flow
    var flowDelegate: ItemDetailFlowDelegate? { get set }
    
    // Outputs
    var item: Driver<RedditItem> { get }
    var imageURL: Driver<URL?> { get }

    // Inputs
    var backTap: AnyObserver<Void> { get }
    
    // Bidirectionals

}

final class ItemDetailViewModel: BaseViewModel, ItemDetailViewModelProtocol {
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    
    // Flow
    weak var flowDelegate: ItemDetailFlowDelegate?
    
    // Outputs
    var item: Driver<RedditItem>
    var imageURL: Driver<URL?>
    
    // Inputs
    let backTap: AnyObserver<Void>
    
    // Bidirectionals
    
    // MARK: Private
    private let disposeBag = DisposeBag()

    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(container: Container, item: RedditItem) {
//        let cacheService = container.resolve(CacheServiceProtocol.self)!
        
        // Outputs
        let itemBr = BehaviorRelay<RedditItem>(value: item)
        self.item = itemBr.asDriver()
        
        let imageURLBr = BehaviorRelay<URL?>(value: nil)
        self.imageURL = imageURLBr.asDriver()
                        
        // Inputs
        let backTapPs = PublishSubject<Void>()
        backTap = backTapPs.asObserver()
        
        // Bidirectionals

        super.init(container: container)
                
        // Bindings
        
        // Image downloading
        itemBr.asObservable()
            .filter { $0.hasImage() }
            .subscribe(onNext: { (item) in
                // Loading image
                var urlImage = URL(fileURLWithPath: "")
                if var imageUrl = URL(string: item.data.url ?? ""), imageUrl.isImage {
                    if imageUrl.absoluteString.starts(with: "//") {
                        imageUrl = URL(string: "https:\(imageUrl.absoluteString)") ?? URL(fileURLWithPath: "")
                    }
                    urlImage = imageUrl
                } else if var imageUrl = URL(string: item.data.thumbnail ?? ""), imageUrl.isImage {
                    if imageUrl.absoluteString.starts(with: "//") {
                        imageUrl = URL(string: "https:\(imageUrl.absoluteString)") ?? URL(fileURLWithPath: "")
                    }
                    urlImage = imageUrl
                }
                if urlImage != URL(fileURLWithPath: "") {
                    imageURLBr.accept(urlImage)
                } else {
                    // Shoudn't be here since I've filtered only items containing an image
                }
            }, onError: { (error) in
                //
                print(error)
            })
            .disposed(by: self.disposeBag)
        
        // Back tap
        backTapPs.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.flowDelegate?.popItemDetail()
            })
            .disposed(by: disposeBag)
    }
}


