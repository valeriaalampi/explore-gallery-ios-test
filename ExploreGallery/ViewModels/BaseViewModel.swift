//
//  BaseViewModel.swift
//  ExploreGallery
//
//  Created by Valeria Alampi on 13/10/2020.
//

import UIKit
import RxSwift
import RxCocoa
import Swinject

protocol BaseViewModelProtocol {
    var isLoading: BehaviorRelay<Bool> { get }
}


class BaseViewModel: BaseViewModelProtocol {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    // Outputs
    let isLoading: BehaviorRelay<Bool>
    
    // Services
    public let container: Container
    
    public lazy var webAPIService: WebAPIServiceProtocol = {
        return self.container.resolve(WebAPIServiceProtocol.self)!
    }()
    
    // Bidirectionals
    
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(container: Container) {
        
        self.container = container
        isLoading = BehaviorRelay<Bool>(value: false)
        
    }
    
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    // Create an alert with one confirm button
    func createAlert(_ title: String, _ message: String, cancel: String) -> UIAlertController {
        return BaseViewModel.createAlert(title, message, cancel: cancel)
    }
    
    // Create an alert with two buttons
    func createAlert(_ title: String, _ message: String, confirm: String, cancel: String, _ confirmHandler: ((UIAlertAction) -> Void)? = nil, _ cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        return BaseViewModel.createAlert(title, message, confirm: confirm, cancel: cancel, confirmHandler, cancelHandler)
    }
    
    
    // MARK: Protocol conformance
    
    //MARK: Static Members
    static func createAlert(_ title: String, _ message: String, cancel: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancel, style: .cancel))
        alert.setTint(color: UIColor.primary)
        alert.customizeTitle(font: UIFont.medium15, color: .black)
        alert.customizeBody(font: UIFont.regular13, color: .systemGreen)
        return alert
    }
    
    // Crea un alert con due pulsanti
    static func createAlert(_ title: String, _ message: String, confirm: String, cancel: String, _ confirmHandler: ((UIAlertAction) -> Void)? = nil, _ cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: confirm, style: .default, handler: confirmHandler))
        alert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: cancelHandler))
        alert.setTint(color: UIColor.primary)
        alert.customizeTitle(font: UIFont.medium15, color: .black)
        alert.customizeBody(font: UIFont.regular13, color: .systemGreen)
        return alert
    }
    
    // Crea un alert con un pulsante di conferma e basta
    static func createAlert(_ title: String, _ message: String, confirm: String, _ confirmHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: confirm, style: .default, handler: confirmHandler))
        alert.setTint(color: UIColor.primary)
        alert.customizeTitle(font: UIFont.medium15, color: .black)
        alert.customizeBody(font: UIFont.regular13, color: .systemGreen)
        return alert
    }
    
    // MARK: Private
    
    private static func showAlert(alert: UIAlertController, animated: Bool) {
        ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! EGNavigationController).present(alert, animated: animated)
    }

}
